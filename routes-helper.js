/**
 * Created by guanChongSheng <12687185@qq.com>
 * 2022/10/31 16:24
 */
const path = require("path")

const VueRoutesHelper = require("vue-routes-helper")

// 实例化
new VueRoutesHelper({
  // 保存路由数据的js地址
  output: path.resolve(__dirname, "./src/router/routes.js"),
  // routes.js默认需要导入的组件
  importLayout: [
    `import layout from "@/layout/layout"`,
    `import wrap from "@/layout/wrap"`
  ],
  // 需要监听并生成路由数据的文件夹
  viewPath: path.resolve(__dirname, "./src/views"),
  // viewPath对应的根路由(默认"/")
  baseUrl: "/",
  // 忽略的文件或者文件夹
  ignore: ["components"],
  // _meta.js内默认component值
  layout: "layout",
  // *.vue文件默认title值. 如果不设置该值,那么默认title是文件名字
  defaultTitle: "默认页面标题",
  // 路由数据更新后,是否在控制台打印消息
  log: true
})
