/**
 * Created by guanChongSheng <12687185@qq.com>
 * 2022/10/24 19:43
 */

const ROUTER_DATA = {
  component: "wrap",
  title: "/1 目录",
  redirect: "/dir-a/a"
}
export default ROUTER_DATA
