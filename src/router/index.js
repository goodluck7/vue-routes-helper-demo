/**
 * Created by guanChongSheng <12687185@qq.com>
 * 2022/11/17 15:08
 * 可以用这种方式引入自动生成的路由数据
 * 请先运行vue-router-helper实例. 以免dev-server运行后,routes.js不存在导致报错
 */

import Vue from "vue"
import Router from "vue-router"
import autoRoutes from "./routes"
import layout from "@/layout/layout"

Vue.use(Router)

// 路由
export const routes = [
    ...autoRoutes,
    {
        path: "*",
        component: layout,
        redirect: "/error/404",
    }
]

const routerIns = new Router({
    routes
})

export default routerIns
