/*
vue-routes-helper
author 12687185@qq.com
路由数据采集时间:2022/11/17 15:42:07
*/

import layout from "@/layout/layout"
import wrap from "@/layout/wrap"

export default [{"component":layout,"path":"/","meta":{"title":"views"},"children":[{"meta":{"title":"标题/a"},"path":"a","component":() => import("@/views/a.vue"),"children":[]},{"meta":{"title":"标题/b"},"path":"b","component":() => import("@/views/b.vue"),"children":[]},{"component":wrap,"path":"dir-a","meta":{"title":"/1 目录"},"redirect":"/dir-a/a","children":[{"meta":{"title":"标题/a"},"path":"a","component":() => import("@/views/dir-a/a.vue"),"children":[]},{"meta":{"title":"标题/b"},"path":"b","component":() => import("@/views/dir-a/b.vue"),"children":[]},{"component":wrap,"path":"dir-a-2","meta":{"title":"/1/2 目录"},"redirect":"/dir-a/dir-a-2/a","children":[{"meta":{"title":"标题/a"},"path":"a","component":() => import("@/views/dir-a/dir-a-2/a.vue"),"children":[]},{"meta":{"title":"标题/b"},"path":"b","component":() => import("@/views/dir-a/dir-a-2/b.vue"),"children":[]},{"component":wrap,"path":"dir-a-2-3","meta":{"title":"/dir-a/dir-a-2/dir-a-2-3/a 目录"},"redirect":"/dir-a/dir-a-2/dir-a-2-3/a","children":[{"meta":{"title":"标题/a"},"path":"a","component":() => import("@/views/dir-a/dir-a-2/dir-a-2-3/a.vue"),"children":[]},{"meta":{"title":"标题/b"},"path":"b","component":() => import("@/views/dir-a/dir-a-2/dir-a-2-3/b.vue"),"children":[]}]}]}]},{"component":wrap,"path":"dir-b","meta":{"title":"/2 目录aaaaa"},"redirect":"/dir-b/a","children":[{"meta":{"title":"标题/a"},"path":"a","component":() => import("@/views/dir-b/a.vue"),"children":[]},{"meta":{"title":"title-d"},"path":"d","component":() => import("@/views/dir-b/d.vue"),"children":[]}]}]}]
