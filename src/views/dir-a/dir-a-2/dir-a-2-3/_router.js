/**
 * Created by guanChongSheng <12687185@qq.com>
 * 2022/10/24 10:04
 */

const ROUTER_DATA = {
  component: "wrap",
  redirect: "/dir-a/dir-a-2/dir-a-2-3/a",
  title: "/dir-a/dir-a-2/dir-a-2-3/a 目录"
}
export default ROUTER_DATA
