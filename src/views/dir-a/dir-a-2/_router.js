/**
 * Created by guanChongSheng <12687185@qq.com>
 * 2022/10/24 10:04
 */

const ROUTER_DATA = {
  component: "wrap",
  redirect: "/dir-a/dir-a-2/a",
  title: "/1/2 目录"
}
export default ROUTER_DATA
