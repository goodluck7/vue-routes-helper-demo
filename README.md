### 项目说明
vue-routes-helper 使用例子<br>
持续监听*.vue和_routes.js文件, 并根据物理路径等信息生成vue路由数据.
<br><br>

#### 安装&运行

```
1. npm install
2. npm run routes-helper 或 node ./routes-helper.js
```
#### 具体配置参数请参考"/routes-helper.js"文件内的注释
